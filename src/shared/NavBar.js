import React from "react";
import {NavLink} from "react-router-dom";
import './styles/NavBar.less';

const NavBar = () => {
    return (
        <nav className={'nav-bar'}>
            <NavLink exact to='/' activeClassName='active'>商城</NavLink>
            <NavLink to='/order' activeClassName='active'>订单</NavLink>
            <NavLink to='/create-goods' activeClassName='active'>添加商品</NavLink>
        </nav>
    );
};

export default NavBar;
