import React from 'react';
import './styles/ProgressMask.less';
import {connect} from "react-redux";

class ProgressMask extends React.Component {
    render() {
        console.log(this.props)
        return (<div className='global-mask' style={{display: this.props.mask.show ? 'block' : 'none'}}>
            <span>正在发送请求！！！</span>
        </div>);
    }
}

const mapStateToProps = state => ({
    mask: state.mask,
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ProgressMask);
