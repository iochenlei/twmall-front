import React from "react";
import NavBar from "./shared/NavBar";
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import './index.less';
import {Route, Switch} from "react-router";
import CreateGoods from "./pages/CreateGoods";
import {store} from './store';
import {Provider} from "react-redux";
import Home from "./pages/Home";
import Order from "./pages/Order";
import ProgressMask from "./shared/ProgressMask";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <NavBar/>
                <Switch>
                    <Route path='/create-goods' component={CreateGoods}/>
                    <Route path='/order' component={Order}/>
                    <Route exact path='/' component={Home}/>
                </Switch>
                <ProgressMask/>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById("root"));
