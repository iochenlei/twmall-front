import React from 'react';
import {connect} from "react-redux";
import {deleteOrder, fetchAllOrders} from "../actions/OrdersAction";
import './styles/Order.less';
import {Link} from "react-router-dom";

class Order extends React.Component {
    componentDidMount() {
        this.props.fetchAllOrders();
    }

    handleDelete(order) {
        this.props.deleteOrder(order.id);
    }

    render() {
        return (<div className='order-wrapper'>
            <table>
                <thead>
                <tr>
                    <th>名字</th>
                    <th>单价</th>
                    <th>数量</th>
                    <th>单位</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                {(()=>{
                    if(this.props.orders.length === 0) return <tr><td colSpan='5'>没有订单，快去商城选购吧！</td></tr>;
                })()}
                {this.props.orders.map(order => {
                    return (
                        <tr>
                            <td>{order.goods.name}</td>
                            <td>{order.goods.price}</td>
                            <td>{order.count}</td>
                            <td>{order.goods.unit}</td>
                            <td>
                                <button onClick={() => this.handleDelete(order)}>删除</button>
                            </td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>);
    }
}

const mapStateToProps = state => ({
    orders: state.orders,
});

const mapDispatchToProps = {
    fetchAllOrders,
    deleteOrder,
};

export default connect(mapStateToProps, mapDispatchToProps)(Order);
