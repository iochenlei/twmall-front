import React from 'react';
import {connect} from "react-redux";
import {fetchAllGoods} from "../actions/FetchGoodsAction";
import './styles/Home.less';
import {MdAddCircleOutline} from 'react-icons/md';
import {addGoodsToOrder} from "../actions/OrdersAction";

class Home extends React.Component {
    componentDidMount() {
        this.props.fetchAllGoods();
    }

    addGoodsToOrders(goods) {
        this.props.addGoodsToOrder(goods.id);
    }

    render() {
        return (
            <div className='goods-list'>
                {
                    this.props.goods.map(item => {
                        return (<div className='goods-item'>
                            <div className='img' style={{backgroundImage:`url(${item.imageUrl})`}}></div>
                            <h1>{item.name}</h1>
                            <span>单价：{item.price}元/{item.unit}</span>
                            <MdAddCircleOutline className='add-btn' onClick={()=>this.addGoodsToOrders(item)}/>
                        </div>);
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    goods: state.allGoods,
});

const mapDispatchToProps = {
    fetchAllGoods,
    addGoodsToOrder
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);