import React from 'react';
import './styles/CreateGoods.less';
import {createGoods} from "../actions/CreateGoodsAction";
import {connect} from "react-redux";


class CreateGoods extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            name: '',
            unit: '',
            price: '',
            imageUrl: ''
        }
    }

    setFiledValue(fieldName, event) {
        this.setState({
            [fieldName]: event.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.createGoods({
            name: this.state.name,
            unit: this.state.unit,
            price: parseFloat(this.state.price),
            imageUrl: this.state.imageUrl
        }, () => {
            this.props.history.push('/');
        });
    }
    
    render() {
        return (
            <div className='create-goods-form-wrapper'>
                <h1>添加商品</h1>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <section>
                        <label>名称:</label>
                        <input type='text' placeholder='名称' onChange={(e)=>this.setFiledValue('name', e)} value={this.state.name}/>
                    </section>
                    <section>
                        <label>价格:</label>
                        <input type='text' placeholder='价格'  onChange={(e)=>this.setFiledValue('price', e)} value={this.state.price}/>
                    </section>
                    <section>
                        <label>单位:</label>
                        <input type='text' placeholder='单位'  onChange={(e)=>this.setFiledValue('unit', e)} value={this.state.unit}/>
                    </section>
                    <section>
                        <label>图片:</label>
                        <input type='text' placeholder='图片'  onChange={(e)=>this.setFiledValue('imageUrl', e)} value={this.state.imageUrl}/>
                    </section>
                    <button type='submit'>提交</button>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = {
    createGoods
};

export default connect(null, mapDispatchToProps)(CreateGoods);