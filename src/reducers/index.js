import {combineReducers} from "redux";
import {fetchGoodsReducer} from "./FetchGoodsReducer";
import {fetchOrdersReducer} from "./FetchOrdersReducer";
import {maskReducer} from "./MaskReducer";

export const rootReducers = combineReducers({allGoods: fetchGoodsReducer, orders: fetchOrdersReducer, mask: maskReducer});
