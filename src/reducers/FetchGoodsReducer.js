import {FETCH_GOODS_SUCCESS} from "../actions/FetchGoodsAction";

export const fetchGoodsReducer = (state=[], action) => {
    switch (action.type) {
        case FETCH_GOODS_SUCCESS:
            return action.goods;
        default:
            return state;
    }
};
