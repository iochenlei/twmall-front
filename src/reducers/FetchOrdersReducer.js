import {FETCH_ORDERS_SUCCESS} from "../actions/OrdersAction";

export const fetchOrdersReducer = (state=[], action) => {
    switch (action.type) {
        case FETCH_ORDERS_SUCCESS:
            return action.orders;
        default:
            return state;
    }
};
