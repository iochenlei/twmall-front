import {
    ADD_ORDER_FAILURE,
    ADD_ORDER_REQUEST,
    ADD_ORDER_SUCCESS,
    DELETE_ORDER_FAILURE,
    DELETE_ORDER_REQUEST, DELETE_ORDER_SUCCESS
} from "../actions/OrdersAction";

export const maskReducer = (state = {show: false}, action) => {
    switch (action.type) {
        case ADD_ORDER_REQUEST:
        case DELETE_ORDER_REQUEST:
            return {show: true};
        case ADD_ORDER_SUCCESS:
        case ADD_ORDER_FAILURE:
        case DELETE_ORDER_FAILURE:
        case DELETE_ORDER_SUCCESS:
            return {show: false};
        default:
            return state;
    }
};
