export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';

export const ADD_ORDER_REQUEST = 'ADD_ORDER_REQUEST';
export const ADD_ORDER_SUCCESS = 'ADD_ORDER_SUCCESS';
export const ADD_ORDER_FAILURE = 'ADD_ORDER_FAILURE';

export const DELETE_ORDER_REQUEST = 'DELETE_ORDER_REQUEST';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';
export const DELETE_ORDER_FAILURE = 'DELETE_ORDER_FAILURE';

export const ORDER_API_URL = 'http://localhost:8080/api/orders';

export const fetchAllOrders = () => {
    return dispatch => {
        dispatch({type: FETCH_ORDERS_REQUEST});
        fetch(ORDER_API_URL)
            .then(response => response.json())
            .then(orders => dispatch({
                type: FETCH_ORDERS_SUCCESS,
                orders
            }))
            .catch(() => dispatch({type: FETCH_ORDERS_FAILURE}))
    };
};

export const addGoodsToOrder = goodsId => {
    return dispatch => {
        dispatch({type: ADD_ORDER_REQUEST});
        fetch(ORDER_API_URL + '/' + goodsId, {method: 'POST'})
            .then(() => dispatch({
                type: ADD_ORDER_SUCCESS,
            }))
            .catch(() => dispatch({type: ADD_ORDER_FAILURE}))
    };
};

export const deleteOrder = orderId => {
    return dispatch => {
        dispatch({type: DELETE_ORDER_REQUEST});
        fetch(ORDER_API_URL + '/' + orderId, {method: 'DELETE'})
            .then(response => {
                if (response.status === 200) {
                    dispatch({type: DELETE_ORDER_SUCCESS});
                    fetchAllOrders()(dispatch);
                } else {
                    dispatch({type: DELETE_ORDER_FAILURE})
                }
            })
            .catch(() => dispatch({type: DELETE_ORDER_FAILURE}));
    }
};