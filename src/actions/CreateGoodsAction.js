const CREATE_GOODS_SUCCESS = 'CREATE_GOODS_SUCCESS';

const createGoodsSuccess = goods => {
    return {
        type: CREATE_GOODS_SUCCESS,
        goods
    }
};

const CREATE_GOODS_API_URL = 'http://127.0.0.1:8080/api/goods';

export const createGoods = (goods, successCallback) => {
    return dispatch => {
        return fetch(CREATE_GOODS_API_URL, {method: 'POST', body: JSON.stringify(goods), headers: {'Content-Type': 'application/json'}})
            .then(response => {
                if(response.status === 201) {
                    dispatch(createGoodsSuccess(goods));
                    successCallback && successCallback();
                }
            })
    }
};
