export const FETCH_GOODS_SUCCESS = 'FETCH_GOODS_SUCCESS';
const FETCH_GOODS_API_URL = 'http://localhost:8080/api/goods';

export const fetchAllGoods = () => {
    return dispatch => {
        fetch(FETCH_GOODS_API_URL)
            .then(response => response.json())
            .then(goods => dispatch({
                type: FETCH_GOODS_SUCCESS,
                goods
            }))
    };
};
